#!/bin/sh
#
# Install script for git.autistici.org/ai3/tutorial-2-app
# inside a Docker container.
#
# The installation procedure requires installing some
# dedicated packages, so we have split it out to a script
# for legibility.

set -e

# Install the ai3-tutorials package repository.
install_packages curl gnupg
echo "deb http://deb.autistici.org/urepo ai3-tutorials/" \
	> /etc/apt/sources.list.d/tutorials.list
curl -s http://deb.autistici.org/repo.key | apt-key add -
apt-get -q update

install_packages ai3-tutorial-2-app

# Remove packages used for installation.
apt-get remove -y --purge curl gnupg
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*

