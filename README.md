Tutorial #2: sample app
=======================

In questo tutorial installeremo un'applicazione di esempio
sull'infrastruttura *ai3*, senza copiare i files da uno schema
predefinito ma scrivendoli ex novo.

Nel caso vi trovaste bloccati, una soluzione possibile si trova nella
directory *solution*, ma sarebbe meglio provare prima tutto il
tutorial autonomamente.

# Setup iniziale

Identico al [Tutorial #1](https://git.autistici.org/ai3/tutorial-1).

Create un nuovo repository su Gitlab, nel vostro namespace utente,
denominato *ai3-tutorial-2*. Questo repository inizialmente sarà vuoto
(non un clone del repository contenente il tutorial).

# Definizione del problema

Abbiamo un'applicazione web, molto semplice (il cui esatto scopo è
irrilevante), e vogliamo installarla su *ai3*. Useremo il meccanismo
di routing HTTP intrinseco ad ai3, per cui non ci sarà bisogno di
preoccuparsi di certificati e simili. Vogliamo esporre l'app con il
nome *app.investici.org* (ricordiamo che investici.org è il dominio di
test usato da ai3/testbed).

L'applicazione si trova alla URL
https://git.autistici.org/ai3/tutorial-2-app e non c'è bisogno di
sapere troppo in dettaglio come funziona, se non in termini della sua
*interfaccia di controllo*, ovvero come si configura e che tipo di
traffico gestisce. 

## Configurazione dell'applicazione di test

L'applicazione di test è un semplice server HTTP. Queste sono le
informazioni che abbiamo su come si configura:

* l'applicazione ha bisogno di un token segreto, che va passato
  tramite la variabile di environment `SECRET_TOKEN`
* l'applicazione parla protocollo HTTP (senza SSL) e si pone in
  ascolto all'avvio sull'indirizzo specificato dalla variabile di
  environment `ADDR`

## Installazione dell'applicazione di test

L'applicazione si installa come un pacchetto Debian, con i seguenti
parametri:

* la *source* da usare per il pacchetto è

        deb http://deb.autistici.org/urepo ai3-tutorials/

* la chiave GPG di quel repository si trova alla URL
  http://deb.autistici.org/repo.key
* il nome del pacchetto è `ai3-tutorial-2-app`
* il binario viene installato in `/usr/bin/tutorial-2-app`

# Costruzione dell'immagine

Con queste informazioni provate a scrivere un Dockerfile, nel vostro
nuovo repository vuoto, per l'applicazione. Si può seguire lo stesso
schema usato nel [Tutorial
#1](https://git.autistici.org/ai3/tutorial-1) ed usare un *build.sh*
per installare il pacchetto Debian. Dato che nell'immagine c'è un
singolo processo, non c'è bisogno di utilizzare Chaperone.

Una cosa importante da considerare è che la configurazione verrà
*dall'esterno* (ovvero da Ansible), non deve essere inclusa
nell'immagine!

Il risultato dovrà essere un'immagine che avvii il server come *entry
point*. Dovrebbe essere possibile testarla in locale in questo modo
(utilizzando dei parametri di configurazione a caso):

    $ docker build -t prova .
    $ docker run -p 7007 --env ADDR=:7007 --env SECRET_TOKEN=boo \
        -t prova:latest

# Pubblicazione dell'immagine sul registry

Si procede come nel [Tutorial #1](https://git.autistici.org/ai3/tutorial-1).
L'immagine risultante avrà un identificativo simile a:

    registry.git.autistici.org/<USER>/ai3-tutorial-2:latest

# Deploy dell'immagine su ai3

Anche qui, si fa riferimento al [Tutorial
#1](https://git.autistici.org/ai3/tutorial-1).

Nuovamente ricordiamo che le modifiche ad *ai3/testbed* non vanno
committate!

Prima di tutto però è necessario definire una nuova credenziale
(ovvero un token segreto): le credenziali in *ai3* sono documentate
così che sia facile rigenerarle quando necessario. Non solo, ma con la
gestione centralizzata dei segreti è possibile creare ambienti di test
senza bisogno di utilizzare segreti pre-esistenti o condivisi (vengono
infatti rigenerati ogni volta), il che semplifica molto le
cose. Vediamo come si fa:

Per aggiungere una nuova credenziale bisogna modificare il file
`conf/passwords.yml`, aggiungendo ad esempio quanto segue:

    - name: ai3_tutorial2_secret
      dscription: Secret for ai3/tutorial-2 test application

questo definirà, in Ansible, una variabile *ai3_tutorial2_secret* che
potremo usare per configurare il servizio.

E' poi importante sapere che, nella definizione dei container in
*services.yml*, si può usare l'attributo *env* per definire variabili
di environment che il container userà a runtime, per esempio:

    containers:
      - name: ...
        image: ...
        env:
          VAR1: value
          VAR2: value

In questo file dovrebbe essere possibile utilizzare la normale
sintassi Ansible per l'espansione delle variabili (`{{ var }}`).

Se alla fine tutto è andato bene, con una richiesta come la seguente:

    $ curl -k --resolve app.investici.org:443:192.168.10.10 \
        https://app.investici.org/

si dovrebbe vedere un messaggio diagnostico che conferma la corretta
configurazione del servizio.

# Extra: Autenticazione

Visto ciò che mostra questa applicazione, sicuramente va protetta in
qualche modo con dell'autenticazione. L'infrastruttura provvede, per
servizi HTTP, un modo piuttosto facile per farlo: basta impostare, per
i *public_endpoints* di un servizio, l'attributo *enable_sso_proxy* a
*true*.

Per verificarne il funzionamento, ricordiamo che l'utente di default
per ai3/testbed è *admin* con password *password*.
